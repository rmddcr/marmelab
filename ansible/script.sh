#!/bin/bash
microk8s.helm3 repo add jetstack https://charts.jetstack.io
microk8s.helm3 repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
microk8s.helm repo add gitlab https://charts.gitlab.io
microk8s.helm3 repo update

# pin the version in the future

microk8s.helm3 upgrade -i  gitlab gitlab/gitlab-runner --namespace gitlab --create-namespace -f runner.yaml

microk8s.helm3 upgrade -i nginx-ingress ingress-nginx/ingress-nginx \
      --namespace ingress --create-namespace \
      --set controller.config.compute-full-forwarded-for="true" \
      --set controller.config.use-forwarded-headers="true" \
      --set controller.dnsPolicy=ClusterFirstWithHostNet \
      --set controller.hostNetwork=true \
      --set controller.kind=DaemonSet \
      --set controller.service.enabled=true \
      --set controller.admissionWebhooks.enabled=false

microk8s.helm3 upgrade -i  cert-manager jetstack/cert-manager --namespace certificate --create-namespace --set installCRDs=true

cat << EOF | microk8s.kubectl apply -f -
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-production
spec:
  acme:
    # Email address used for ACME registration
    email: damitha@local.host
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      # Name of a secret used to store the ACME account private key
      name: letsencrypt-production-private-key
    # Add a single challenge solver, HTTP01 using nginx
    solvers:
    - http01:
        ingress:
          class: nginx
EOF
