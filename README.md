## Ansible Playbook
An Ansible playbook is a configuration management tool that allows you to automate the deployment and configuration of software and infrastructure. Playbooks are written in YAML format and consist of a series of tasks and roles that define the steps necessary to achieve a desired state. Ansible playbooks are useful for managing complex systems with multiple nodes, and can be used for tasks such as provisioning servers, managing applications, and deploying updates.

## HELM Chart Installation
Helm is a package manager for Kubernetes that simplifies the deployment and management of applications. A Helm chart is a collection of files that define the resources necessary to run a Kubernetes application. To install a Helm chart, you first need to add the chart repository to your local Helm installation. Once you have added the repository, you can use the helm install command to deploy the chart. Helm charts are useful for managing complex applications with multiple components and dependencies.

## Raw Manifests Installation
Raw manifests are Kubernetes configuration files that define the desired state of a Kubernetes resource. Manifests can be written in YAML or JSON format and are used to create, update, and delete Kubernetes resources. To install a Kubernetes resource using raw manifests, you first need to create the manifest file. Once you have created the manifest file, you can use the kubectl apply command to apply the changes to your Kubernetes cluster. Raw manifests are useful for managing individual Kubernetes resources or for making small changes to existing resources.


## Steps by stem guide to setup the server 

1. Update the inventory file with the server ip and make sure the ssh login has been configured to the server executing the ansible script
2. Update the `marmelab/helm/app/values.yaml` with a valid dns name
3. Run `cd marmelab/ansible` 
4. Run `ansible-playbook install-microk8s.yaml -i inventory`


* Befor running the below please update the `sensitive-data/runner.yaml` with the updated runnerRegistrationToken and gitlabUrl values


5. Run `ansible-playbook install-helm-charts.yaml -i ../../sensitive-data/inventory`
   
6. Run `ansible-playbook disable-auto-updates.yaml -i ../../sensitive-data/inventory `
7. Run `microk8s.kubectl create clusterrolebinding gitlab-is-now-cluster-admin --clusterrole=cluster-admin --serviceaccount=gitlab:default`

* `marmelab/.gitlab-ci.yml` contains the ci deffinition for gitllab piple line

## Local development 
For local development please refer the docker compose files located at `marmelab/docker-compose`
1. Use the `docker-compose run test` invoke unit tests locally
2. Use the `docker-compose up dev` run the application locally
3. Use the `docker-compose -f docker-compose.production.yaml up` run the prod build locally



